package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {
Button btn3;
Bundle b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent mainIntent=   getIntent();
         b = mainIntent.getExtras();

        btn3=findViewById(R.id.btn3);

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(b!=null){

                  //  String datafromMan=
                   // b.get
                    Log.d("TAG", "onClick: "+b);
                    btn3.setText((String)b.get("ID"));
                }
            }
        });
    }
}
