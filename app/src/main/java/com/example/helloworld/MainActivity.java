package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
Button btn,btn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn= findViewById(R.id.middlebtn);
        btn1=findViewById(R.id.disablebtn);
        btn1.setVisibility(View.INVISIBLE);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"FaQ",Toast.LENGTH_LONG).show();
                btn1.setVisibility(View.VISIBLE);
                btn1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Main = From
                        //Second = to (Where)
                      //  Intent hi = new Intent(MainActivity.this,SecondActivity.class);

                        Intent hello = new Intent(MainActivity.this,SecondActivity.class);
String userdatafromMain = "MAINDATA";
hello.putExtra("ID",userdatafromMain);

                        finish();
                        startActivity(hello);
                    }
                });
            }
        });
    }
}
